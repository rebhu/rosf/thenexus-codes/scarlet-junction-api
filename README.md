# Scarlet Junction API

This is an application API that allows functionality to help establish connection with Scarlet 

### Features

- [ ] User Authentication
- [ ] Scarlet
  - [ ] Start a server
  - [ ] Fetch Scarlet instance IP
  - [ ] Execute a command on this newly started server
- [ ] Setup reverse SSH for accessing remote computer

### TODO